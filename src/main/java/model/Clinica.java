package model;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class Clinica {

    private List<Turno> turnos;
    private List<Medico> medicos;

    public Clinica() {
        this.turnos = new ArrayList<Turno>();
        this.medicos = new ArrayList<Medico>();
    }

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void agregarTurno(Paciente paciente, String especialidad, LocalDate fecha) throws Exception {
        Turno turno = new Turno();
        turno.setFecha(fecha);
        turno.setMedico(this.buscarMedicoSegunEspecialidad(especialidad));
        turno.setPaciente(paciente);
    }

    private Medico buscarMedicoSegunEspecialidad(String especialidad) throws Exception {
        Optional<Medico> medicoAux = this.medicos.stream().filter(m -> m.getEspecialidad().equals(especialidad)).findFirst();
        if(medicoAux.isPresent()){
            return medicoAux.get();
        }
        throw new Exception("No existe un medico para la especialidad: " + especialidad);
    }
}
